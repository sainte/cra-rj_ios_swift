//
//  CRA_RJUITests.swift
//  CRA-RJUITests
//
//  Created by Tiago Bencardino on 26/02/2016.
//  Copyright © 2016 MobWiz. All rights reserved.
//

import XCTest

class CRA_RJUITests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        let app = XCUIApplication()
        setupSnapshot(app)
        XCUIApplication().launch()
        
        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testScreenshots() {
        
        let app = XCUIApplication()
        
        let tabBarsQuery = app.tabBars
        snapshot("01_radio_screen")
        
        tabBarsQuery.buttons.element(boundBy: 1).tap()
        snapshot("02_noticias_screen")
        
        tabBarsQuery.buttons.element(boundBy: 2).tap()
        snapshot("03_contato_screen")
        
        tabBarsQuery.buttons.element(boundBy: 3).tap()
        snapshot("03_sobre_screen")
        
    }
}
