//
//  SecondViewController.swift
//  CRA-RJ
//
//  Created by Tiago Bencardino on 26/02/2016.
//  Copyright © 2016 MobWiz. All rights reserved.
//

import UIKit

class RSSViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, RSSManagerDelegate {
    
    var rssManager: RSSManager!
    var items : [RSSItem] = []
    var refreshControl = UIRefreshControl()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.configureRefreshControl()
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        self.title = "Notícias"
        
        self.rssManager = RSSManager(delegate: self)
        self.items = rssManager.offlineItems()
        
        self.refreshControl.beginRefreshing()
    }
    
    func configureRefreshControl() {
        
        self.refreshControl.addTarget(self, action: #selector(RSSViewController.refresh), for: .valueChanged)
        self.tableView.addSubview(refreshControl)
    }
    
    func refresh () {
        
        self.rssManager.reload()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "contentCell")
        let item:RSSItem = items[indexPath.row]
        
        let dayLabel = cell?.viewWithTag(1) as! UILabel
        let monthLabel = cell?.viewWithTag(2) as! UILabel
        let titleLabel = cell?.viewWithTag(3) as! UILabel
        
        let outputDate = DateFormatter()
        let brLocale = Locale(identifier: "pt_BR")
        outputDate.locale = brLocale
        
        outputDate.dateFormat = "dd"
        dayLabel.text = outputDate.string(from: item.date! as Date)
        
        outputDate.dateFormat = "MMM"
        monthLabel.text = outputDate.string(from: item.date! as Date)
        
        titleLabel.text = item.title
        
        return cell!
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let cell = sender as! UITableViewCell
        let indexPath = tableView.indexPath(for: cell)
        tableView.deselectRow(at: indexPath!, animated: true)
        let item = items[(indexPath?.row)!]
        
        let webVC = segue.destination as! WebViewController
        webVC.item = item
    }
    
    func RSSManagerFinished(_ items: [RSSItem], success: Bool) {
        
        if (success) {
            self.items = items
            self.tableView.reloadData()
            self.refreshControl.endRefreshing()
        } else {
            self.refreshControl.endRefreshing()
        }
    }
}
