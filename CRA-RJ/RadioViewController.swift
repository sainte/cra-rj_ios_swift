//
//  FirstViewController.swift
//  CRA-RJ
//
//  Created by Tiago Bencardino on 26/02/2016.
//  Copyright © 2016 MobWiz. All rights reserved.
//

import UIKit
import FontAwesome_swift

class RadioViewController: UIViewController {
    
    var player = MWPlayer(urlString: "http://painel.localmidia.com.br:8076/aovivo")
    
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var pauseButton: UIButton!
    @IBOutlet weak var stopButton: UIButton!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        configureButtons()
        //        player.stop()
    }
    
    func configureButtons() {
        
        configureButton(playButton, icon: .play)
        configureButton(pauseButton, icon: .pause)
        configureButton(stopButton, icon: .stop)
    }
    
    func configureButton(_ button:UIButton, icon:FontAwesome) {
        
        let size = CGSize(width: 50, height: 50)
        
        let image = UIImage.fontAwesomeIcon(name: icon, textColor: Styling.blueColor, size: size)
        let imageHighlighted = UIImage.fontAwesomeIcon(name: icon, textColor: Styling.orangeColor, size: size)
        
        button.setImage(image, for: .normal)
        button.setImage(imageHighlighted, for: .highlighted)
    }
    
    @IBAction func playTapped(_ sender: AnyObject) {
        
        player.play()
    }
    
    @IBAction func pauseTapped(_ sender: AnyObject) {
        
        player.pause()
    }
    
    @IBAction func stopTapped(_ sender: AnyObject) {
        
        player.stop()
    }
    
}

