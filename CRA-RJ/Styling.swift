//
//  Styling.swift
//  CRA-RJ
//
//  Created by Tiago Bencardino on 28/02/2016.
//  Copyright © 2016 MobWiz. All rights reserved.
//

import UIKit

class Styling: NSObject {
    
    static let blueColor = UIColor(red: 36/255.0, green: 68/255.0, blue: 132/255.0, alpha: 1.0)
    static let orangeColor = UIColor(red: 216/255.0, green: 113/255.0, blue: 39/255.0, alpha: 1.0)
    
    static func defaults() {
        
        let navBarAppearance = UINavigationBar.appearance()
        navBarAppearance.titleTextAttributes = [NSFontAttributeName: UIFont(name: "AvenirNext-Regular", size: 16)!,  NSForegroundColorAttributeName: Styling.blueColor]
        
        let tabBarAppearance = UITabBarItem.appearance()
        tabBarAppearance.setTitleTextAttributes([NSFontAttributeName: UIFont(name: "AvenirNext-Regular", size: 10)!], for: UIControlState())
    }
    
    static func roundView(_ view: UIView) {
        
        view.layer.cornerRadius = view.frame.size.height / 2
        view.clipsToBounds = true
    }
    
    static func roundViewWithCorner(_ corner: CGFloat, view: UIView) {
        
        view.layer.cornerRadius = corner
    }
}
