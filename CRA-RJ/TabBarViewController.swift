//
//  TabBarViewController.swift
//  CRA-RJ
//
//  Created by Tiago Bencardino on 26/02/2016.
//  Copyright © 2016 MobWiz. All rights reserved.
//

import UIKit
import FontAwesome_swift

class TabBarViewController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var icons = [FontAwesome.microphone, FontAwesome.rss, FontAwesome.user, FontAwesome.info]
        
        self.tabBar.isTranslucent = false
        //        self.tabBar.barTintColor = UIColor.blackColor()
        self.tabBar.tintColor = Styling.blueColor
        
        for i in 0..<self.viewControllers!.count {
            
            let tabBarItem = self.viewControllers![i].tabBarItem
            tabBarItem?.image = UIImage.fontAwesomeIcon(name: icons[i], textColor: Styling.blueColor, size: CGSize(width: 30, height: 30))
        }
    }
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
