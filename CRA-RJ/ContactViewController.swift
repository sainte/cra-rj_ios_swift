//
//  ContactViewController.swift
//  CRA-RJ
//
//  Created by Tiago Bencardino on 26/02/2016.
//  Copyright © 2016 MobWiz. All rights reserved.
//

import UIKit
import MessageUI

class ContactViewController: UIViewController, UITextViewDelegate, MFMailComposeViewControllerDelegate {
    
    
    @IBOutlet weak var subjectTextField: UITextView!
    @IBOutlet weak var nameTextField: UITextView!
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var heightMessageConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightContentViewConstraint: NSLayoutConstraint!
    
    var activeField: UITextView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        preloadName()
        roundViews()
        self.heightContentViewConstraint.constant = UIScreen.main.bounds.width
        // Keyboard stuff.
        let center: NotificationCenter = NotificationCenter.default
        center.addObserver(self, selector: #selector(ContactViewController.keyboardWasShown(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        center.addObserver(self, selector: #selector(ContactViewController.keyboardWillBeHidden(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func preloadName() {
        
        let defaults = UserDefaults.standard
        if let _ = defaults.object(forKey: "name") {
            nameTextField.text = defaults.object(forKey: "name") as! String
        }
    }
    
    func roundViews() {
        
        Styling.roundViewWithCorner(5, view: subjectTextField)
        Styling.roundViewWithCorner(5, view: nameTextField)
        Styling.roundViewWithCorner(5, view: messageTextView)
        Styling.roundViewWithCorner(5, view: sendButton)
    }
    
    func keyboardWasShown(_ notification: Notification) {
        
        //Need to calculate keyboard exact size due to Apple suggestions
        self.scrollView.isScrollEnabled = true
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize!.height, 0.0)
        
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        
        if let _ = activeField
        {
            let bottomPoint = CGPoint(x: activeField!.frame.origin.x, y: activeField!.frame.size.height + activeField!.frame.origin.y)
            if (!aRect.contains(bottomPoint))
            {
                self.scrollView.scrollRectToVisible(activeField!.frame, animated: true)
            }
        }
    }
    
    func keyboardWillBeHidden(_ notification: Notification) {
        
        //Once keyboard disappears, restore original positions
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, -keyboardSize!.height, 0.0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        self.view.endEditing(true)
        self.scrollView.isScrollEnabled = true
        
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        
        activeField = textView
        
        return true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    @IBAction func sendTapped(_ sender: AnyObject) {
        
        sendEmail()
    }
    override func viewWillDisappear(_ animated: Bool) {
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    //MARK: email
    func sendEmail() {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["webradio@cra-rj.org.br"])
            mail.setMessageBody("<p>\(messageTextView.text)</p><p>Enviado por: \(nameTextField.text) via iOS Web Radio</p>", isHTML: true)
            mail.setSubject(subjectTextField.text)
            present(mail, animated: true, completion: nil)
        } else {
            UIAlertView.init(title: "Ops!", message: "Não podemos enviar agora. Verifique sua conexão e as suas configurações de email.", delegate: self, cancelButtonTitle: "OK").show()
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
        controller.dismiss(animated: true, completion: nil)
        let defaults = UserDefaults.standard
        defaults.set(nameTextField.text, forKey: "name")
    }
    
    
}
