//
//  AboutViewController.swift
//  CRA-RJ
//
//  Created by Tiago Bencardino on 26/02/2016.
//  Copyright © 2016 MobWiz. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func openCrarj(_ sender: AnyObject) {
        
        UIApplication.shared.openURL(URL(string: "http://www.cra-rj.org.br/")!)
    }
    
    @IBAction func openLocalmidia(_ sender: AnyObject) {
        
        UIApplication.shared.openURL(URL(string: "http://www.localmidia.com.br")!)
    }
}
