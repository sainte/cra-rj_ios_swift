//
//  RSSItem.swift
//  CRA-RJ
//
//  Created by Tiago Bencardino on 19/03/2016.
//  Copyright © 2016 MobWiz. All rights reserved.
//

import Foundation
import RealmSwift
import MWFeedParser

class RSSItem: Object {
    
    dynamic var title : String?
    dynamic var date : Date?
    dynamic var link : String?
    
    func feedToItem(_ feedItem: MWFeedItem) {
        
        title = feedItem.title
        date = feedItem.date
        link = feedItem.link
    }
    // Specify properties to ignore (Realm won't persist these)
    
    //  override static func ignoredProperties() -> [String] {
    //    return []
    //  }
}
