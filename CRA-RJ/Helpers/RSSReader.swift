//
//  RSSReader.swift
//  CRA-RJ
//
//  Created by Tiago Bencardino on 28/02/2016.
//  Copyright © 2016 MobWiz. All rights reserved.
//

import Foundation
import MWFeedParser

protocol RSSReaderDelegate {
    
    func RSSReaderFinished(_ items:[AnyObject], success:Bool)
}

class RSSReader: NSObject, MWFeedParserDelegate {
    
    var urlString = ""
    var feedParser:MWFeedParser
    var delegate:RSSReaderDelegate
    
    var items: NSMutableArray = []
    init(urlString: String, delegate: RSSReaderDelegate) {
        
        self.urlString = urlString
        self.delegate = delegate
        self.feedParser = MWFeedParser(feedURL: URL(string: urlString))
        super.init()
        
        feedParser.delegate = self
        feedParser.feedParseType = ParseTypeFull
        feedParser.connectionType = ConnectionTypeAsynchronously
        
        feedParser.parse()
    }
    
    func reload() {
        
        items = []
        feedParser.parse()
    }
    
    //MARK: MWFeedParserDelegate
    
    func feedParserDidStart(_ parser: MWFeedParser!) {
        
    }
    
    func feedParser(_ parser: MWFeedParser!, didParseFeedInfo info: MWFeedInfo!) {
        
    }
    
    func feedParser(_ parser: MWFeedParser!, didParseFeedItem item: MWFeedItem!) {
        
        items.add(item)
    }
    
    func feedParserDidFinish(_ parser: MWFeedParser!) {
        
        self.delegate.RSSReaderFinished(self.items as [AnyObject], success: true)
    }
    
    func feedParser(_ parser: MWFeedParser!, didFailWithError error: NSError!) {
        
        self.delegate.RSSReaderFinished([], success: false)
    }
}
