//
//  MWPlayer.swift
//  CRA-RJ
//
//  Created by Tiago Bencardino on 28/02/2016.
//  Copyright © 2016 MobWiz. All rights reserved.
//

import Foundation
import AVFoundation
import MediaPlayer
import MWFeedParser

class MWPlayer: NSObject {
    
    static var player: AVPlayer?
    var urlString = ""
    
    init(urlString: String) {
        
        super.init()
        self.urlString = urlString;
        self.load()
    }
    
    func load() {
        
        let playerItem = AVPlayerItem(url: URL(string: urlString)!)
        MWPlayer.player = AVPlayer(playerItem: playerItem)
        MWPlayer.player!.rate = 1.0
    }
    
    func play() {
        
        if MWPlayer.player == nil {
            self.load()
        }
        MWPlayer.player!.play()
    }
    
    func pause() {
        
        MWPlayer.player?.pause()
    }
    
    func stop() {
        
        MWPlayer.player = nil
    }
    
    static func registerForBackgroundAudio() {
        
        UIApplication.shared.beginReceivingRemoteControlEvents()
        
        let audioSession = AVAudioSession.sharedInstance()
        //        do {
        try! audioSession.setCategory(AVAudioSessionCategoryPlayback)
        try! audioSession.setActive(true)
        //        } catch _ {
        
        //        }
        
        //set artwork
        let mediaArtwork = MPMediaItemArtwork(image: UIImage(named: "Logo")!)
        MPNowPlayingInfoCenter.default().nowPlayingInfo = [MPMediaItemPropertyArtwork : mediaArtwork, MPMediaItemPropertyTitle : "CRA-RJ"]
        
        NotificationCenter.default.addObserver(self, selector: #selector(MWPlayer.musicPlayerPlayBackStatusChanged(_:)), name: NSNotification.Name.MPMusicPlayerControllerPlaybackStateDidChange, object: nil)
        MPMusicPlayerController.systemMusicPlayer().beginGeneratingPlaybackNotifications()
    }
    
    func musicPlayerPlayBackStatusChanged(_ notification : Notification) {
        
        switch MPMusicPlayerController.systemMusicPlayer().playbackState {
        case .paused :
            break
        default:
            break
        }
    }
}
