//
//  RSSManager.swift
//  CRA-RJ
//
//  Created by Tiago Bencardino on 19/03/2016.
//  Copyright © 2016 MobWiz. All rights reserved.
//

import Foundation
import RealmSwift
import MWFeedParser

protocol RSSManagerDelegate {
    
    func RSSManagerFinished(_ items:[RSSItem], success:Bool)
}

class RSSManager : NSObject, RSSReaderDelegate {
    
    var reader: RSSReader!
    let realm = try! Realm()
    var delegate : RSSManagerDelegate?
    
    init(delegate: RSSManagerDelegate) {
        
        super.init()
        
        self.delegate = delegate
        reader = RSSReader(urlString: "http://www.cra-rj.adm.br/rss/", delegate:self)
    }
    
    func offlineItems() -> [RSSItem] {
        
        let items = realm.objects(RSSItem)
        return Array(items) //slow
    }
    
    func reload() {
        
        reader.reload()
    }
    
    func RSSReaderFinished(_ items: [AnyObject], success: Bool) {
        
        if (success) {
            
            let itemList = List<RSSItem>()
            
            for item in items {
                
                let rssItem = RSSItem()
                rssItem.feedToItem(item as! MWFeedItem)
                
                itemList.append(rssItem)
            }
            
            try! realm.write { () -> Void in
                realm.deleteAll()
                realm.add(itemList)
            }
            self.delegate!.RSSManagerFinished(Array(itemList), success: success)
            
        } else {
            
            self.delegate!.RSSManagerFinished(offlineItems(), success: success)
        }
        
    }
}
