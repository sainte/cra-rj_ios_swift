//
//  WebViewController.swift
//  CRA-RJ
//
//  Created by Tiago Bencardino on 28/02/2016.
//  Copyright © 2016 MobWiz. All rights reserved.
//

import UIKit

class WebViewController: UIViewController, UIWebViewDelegate {
    
    @IBOutlet weak var webView: UIWebView!
    var item: RSSItem!
    var activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let urlString = item.link!.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        let requestObj = URLRequest(url: URL(string: urlString!)!, cachePolicy: .returnCacheDataElseLoad, timeoutInterval: 10.0)
        self.webView.scalesPageToFit = true
        self.webView.loadRequest(requestObj)
        self.webView.delegate = self
        
        activityIndicator.startAnimating()
        
        self.title = item.title
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: activityIndicator)
        self.navigationController?.navigationBar.tintColor = Styling.blueColor
        
        self.automaticallyAdjustsScrollViewInsets = true
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        
        self.activityIndicator.isHidden = true
        self.activityIndicator.stopAnimating()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        
        self.activityIndicator.isHidden = true
        self.activityIndicator.stopAnimating()
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
    }
}
